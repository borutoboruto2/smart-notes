package fr.samadou.smartnotesbackend.service;

import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.enums.NoteCategory;
import fr.samadou.smartnotesbackend.repository.NoteRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

/**
 * @author sareaboudousamadou.
 */
@RunWith(MockitoJUnitRunner.class)
public class NoteServiceTest {
    @Mock
    private NoteRepository noteRepository;

    @InjectMocks
    private NoteService noteService;

    @Test
    public void getAllNotesTest() {
        //given
        List<Note> notes = new ArrayList<>();
        notes.add(
                Note.builder()
                        .description("alias")
                        .title("lister les alias d'une commande")
                        .id(1L)
                        .build());
        notes.add(
                Note.builder()
                        .description("ps -fu <user>")
                        .title("lister les processus d'un user\"")
                        .id(1L)
                        .build());
        Mockito.when(noteRepository.findAll()).thenReturn(notes);

        //when
        List<Note> notesReturned = noteService.getAllNotes();
        //then
        Assertions.assertThat(notesReturned)
                .isNotEmpty().hasSize(2);
    }

    @Test
    public void addNewNoteTest() {
        //given
        Note newNote = Note.builder().build();
        Mockito.when(noteRepository.save(any(Note.class))).thenReturn(newNote);

        //when
        Note noteAdded = noteService.addNewNote(newNote);

        //then
        Assertions.assertThat(noteAdded).isEqualTo(newNote);
    }

    @Test
    public void findNoteById() {
        //given
        Note noteToReturn = Note.builder()
                .id(1L)
                .title("Junit")
                .description("Junit is awesome")
                .category(NoteCategory.JAVA)
                .build();
        Mockito.when(noteRepository.findById(anyLong())).thenReturn(Optional.of(noteToReturn));

        //when
        Note noteReturn = noteService.findNoteById(1L);

        ///then
        Assertions.assertThat(noteReturn).isNotNull();
        Assertions.assertThat(noteReturn).isEqualTo(noteToReturn);
    }

}
