package fr.samadou.smartnotesbackend.config;

import fr.samadou.smartnotesbackend.webservice.NoteWebService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HttpDestinationFactory;
import org.apache.cxf.transport.servlet.ServletDestinationFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * @author sareaboudousamadou.
 */
@Configuration
@ComponentScan("fr.samadou.smartnotesbackend")
public class TestConfig {

    private static final String SERVICE_URL = "http://localhost:8080/service/Note";

    @Bean("noteWebServiceClient")
    public NoteWebService noteWebServiceClient() {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        jaxWsProxyFactoryBean.setServiceClass(NoteWebService.class);
        jaxWsProxyFactoryBean.setAddress(SERVICE_URL);
        return (NoteWebService) jaxWsProxyFactoryBean.create();
    }
    @Bean(name= Bus.DEFAULT_BUS_ID)
    public SpringBus springBus(LoggingFeature loggingFeature) {
        SpringBus bus = new  SpringBus();
        ServletDestinationFactory destinationFactory = new ServletDestinationFactory();
        bus.setExtension(destinationFactory, HttpDestinationFactory.class);
        bus.getFeatures().add(loggingFeature);
        return bus;
    }
    @Bean
    public LoggingFeature loggingFeature() {
        LoggingFeature loggingFeature = new LoggingFeature();
        loggingFeature.setPrettyLogging(true);
        return loggingFeature;
    }
    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(loggingFeature()), noteWebServiceClient());
        endpoint.publish(SERVICE_URL);
        return endpoint;
    }
}
