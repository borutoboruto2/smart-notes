package fr.samadou.smartnotesbackend.webservice;

import fr.samadou.smartnotesbackend.SmartNotesBackendApplication;
import fr.samadou.smartnotesbackend.config.TestConfig;
import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.service.NoteService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

/**
 * @author sareaboudousamadou.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class NoteWebserviceTest {


    @Autowired
    @Qualifier("noteWebServiceClient")
    private NoteWebService noteWebService;

    @Test
    public void testNoteServiceRequest() {

        List<Note> response = noteWebService.fetchNotes();
        Assertions.assertThat(response).hasSize(0);

    }
}
