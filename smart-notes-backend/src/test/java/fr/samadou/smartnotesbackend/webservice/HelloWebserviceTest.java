package fr.samadou.smartnotesbackend.webservice;

import fr.samadou.smartnotesbackend.SmartNotesBackendApplication;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.client.core.WebServiceTemplate;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

/**
 * @author sareaboudousamadou.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SmartNotesBackendApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HelloWebserviceTest {

    @Rule
    public OutputCapture output = new OutputCapture();

    private WebServiceTemplate webServiceTemplate = new WebServiceTemplate();

    @LocalServerPort
    private int port;

    @Before
    public void setUp() {
        this.webServiceTemplate.setDefaultUri("http://localhost:" + this.port + "/Service/Hello");
    }

    @Test
    public void testHelloRequest() {

        // final String request =
        // "<q0:sayHello xmlns:q0=\"http://service.ws.sample\">Elan</q0:sayHello>";
        String request = "<q0:sayHello xmlns:q0=\"http://webservice.smartnotesbackend.samadou.fr/\"><myname>Elan</myname></q0:sayHello>";

        StreamSource source = new StreamSource(new StringReader(request));
        StreamResult result = new StreamResult(System.out);

        this.webServiceTemplate.sendSourceAndReceiveToResult(source, result);
        assertThat(this.output.toString(),
                containsString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                        + "<ns2:sayHelloResponse xmlns:ns2=\"http://webservice.smartnotesbackend.samadou.fr/\">"
                        + "<return>Hello, Welcome to CXF Spring boot Elan!!!</return>"
                        + "</ns2:sayHelloResponse>"));

    }
}
