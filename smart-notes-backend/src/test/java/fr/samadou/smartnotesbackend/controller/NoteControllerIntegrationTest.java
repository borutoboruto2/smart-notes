package fr.samadou.smartnotesbackend.controller;

import fr.samadou.smartnotesbackend.config.TestConfig;
import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.service.NoteService;
import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static io.restassured.RestAssured.port;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;

/**
 * @author sareaboudousamadou.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NoteControllerIntegrationTest {

    @LocalServerPort
    private int localPort;

    @MockBean
    private NoteService noteService;

    @Before
    public void setUp() {
        port = localPort;
    }

    @Test
    public void shouldReturnAllNotes() {
        given(noteService.getAllNotes()).willReturn(Arrays.asList(
                Note.builder()
                        .description("descritption")
                        .title("title")
                        .id(1L)
                        .build()));

        when().get("/notes").then()
                .statusCode(HttpStatus.OK.value());
    }
}
