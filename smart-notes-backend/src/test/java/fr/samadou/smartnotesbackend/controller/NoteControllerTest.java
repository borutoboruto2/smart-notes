package fr.samadou.smartnotesbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.service.NoteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Arrays;

import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author sareaboudousamadou.
 */
@RunWith(SpringRunner.class)
@WebMvcTest(NoteController.class)
public class NoteControllerTest {

    @MockBean
    private NoteService noteService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_return_all_notes_when_notes_endpoint_is_hit() throws Exception {
        //Given
        given(noteService.getAllNotes()).willReturn(Arrays.asList(Note.builder()
                .description("descritption")
                .title("title")
                .id(1L)
                .build()));
        //when
        //Then
        this.mockMvc.perform(MockMvcRequestBuilders.get("/notes")).andExpect(status().isOk());
    }

    @Test
    public void should_return_bad_request_if_no_note_when_create_new_note() throws Exception {
       //given
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/notes")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsString(null));
        //when
        this.mockMvc.perform(builder)
                //then
        .andExpect(status().isBadRequest());
    }

    @Test
    public void should_add_new_note() throws Exception {
        //given
        Note newNote = Note.builder().title("Linux").description("Linux is an OS").build();
        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put("/notes")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsString(newNote));
        //when
        this.mockMvc.perform(builder)
                //then
                .andExpect(status().isCreated());
    }

    @Test
    public void should_return_note_with_given_id() throws Exception {
        //given
        given(noteService.findNoteById(1L)).willReturn(Note.builder()
                .description("descritption")
                .title("title")
                .id(1L)
                .build());

        //when
        ResultActions result = this.mockMvc.perform(MockMvcRequestBuilders.get("/notedds", 1L)
        .accept(MediaType.APPLICATION_JSON));

        //then
        result.andExpect(status().isOk());
    }


}
