package fr.samadou.smartnotesbackend.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.samadou.smartnotesbackend.enums.NoteCategory;
import lombok.*;
import org.springframework.data.annotation.Id;

import javax.annotation.processing.Generated;

/**
 * @author sareaboudousamadou.
 */
@Data
@Builder
@AllArgsConstructor
public class Note {

    @Id
    private Long id;
    private String title;
    @JsonProperty("content")
    private String description;
    private NoteCategory category;
}
