package fr.samadou.smartnotesbackend.repository;

import fr.samadou.smartnotesbackend.domain.Note;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author sareaboudousamadou.
 */
@Repository
public interface NoteRepository extends MongoRepository<Note, Long> {
}
