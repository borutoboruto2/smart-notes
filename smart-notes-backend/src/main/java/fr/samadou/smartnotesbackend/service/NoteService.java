package fr.samadou.smartnotesbackend.service;

import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author sareaboudousamadou.
 */
@Service
public class NoteService {

    @Autowired
    private NoteRepository noteRepository;

    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }

    public Note addNewNote(Note newNote) {
        Objects.requireNonNull(newNote, "A message to add should not be null");
        return noteRepository.save(newNote);
    }

    public Note findNoteById(final long id) {
        Optional<Note> note = noteRepository.findById(id);

        if(note.isEmpty()) {
            return null;
        }

        return note.get();
    }
}
