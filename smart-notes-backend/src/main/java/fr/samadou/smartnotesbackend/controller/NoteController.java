package fr.samadou.smartnotesbackend.controller;

import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author sareaboudousamadou.
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class NoteController {

    @Autowired
    private NoteService noteService;

    @GetMapping("/notes")
    public ResponseEntity<List<Note>> getAllNotes(){

        return new ResponseEntity<>(noteService.getAllNotes(), HttpStatus.OK);
    }

    @PutMapping(value = "/notes", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Note> addNewNote(@RequestBody Note note) {
        if(note == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(noteService.addNewNote(note), HttpStatus.CREATED);
    }
}
