package fr.samadou.smartnotesbackend.enums;

/**
 * @author sareaboudousamadou.
 */
public enum NoteCategory {
    JAVA, LINUX
}
