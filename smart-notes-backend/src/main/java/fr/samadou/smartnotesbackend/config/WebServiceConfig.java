package fr.samadou.smartnotesbackend.config;

import fr.samadou.smartnotesbackend.interceptors.AppInboundInterceptor;
import fr.samadou.smartnotesbackend.interceptors.AppOutboundInterceptor;
import fr.samadou.smartnotesbackend.service.NoteService;


import javax.annotation.PostConstruct;
import javax.xml.ws.Endpoint;

import fr.samadou.smartnotesbackend.webservice.HelloPortImpl;
import fr.samadou.smartnotesbackend.webservice.NoteWebServiceImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.feature.Feature;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * @author sareaboudousamadou.
 */
@Configuration
public class WebServiceConfig {

    @Autowired
    private Bus bus;

    @Autowired
    private NoteService noteService;

    @Autowired
    private NoteWebServiceImpl noteWebService;

    @Autowired
    private HelloPortImpl helloPort;

    @Autowired
    private AppInboundInterceptor inboundInterceptor;

    @Autowired
    private AppOutboundInterceptor outboundInterceptor;

    @PostConstruct
    public void postConstruct() {
        this.bus.getFeatures().add(this.getLoggingFeature());
        this.bus.getInInterceptors().add(this.inboundInterceptor);
        this.bus.getOutInterceptors().add(this.outboundInterceptor);
    }

    @Bean
    public Endpoint helloEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(this.bus, this.helloPort);
        endpoint.publish("/Hello");
        return endpoint;
    }

    @Bean
    public Endpoint noteEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(this.bus, this.noteWebService);
        endpoint.publish("/Note");
        return endpoint;
    }

    @Bean
    public LoggingFeature getLoggingFeature() {
        return new LoggingFeature();
    }

}
