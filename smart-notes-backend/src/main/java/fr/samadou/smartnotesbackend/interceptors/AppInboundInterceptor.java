package fr.samadou.smartnotesbackend.interceptors;

import org.apache.cxf.ext.logging.LoggingInInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.InInterceptors;
import org.apache.cxf.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author sareaboudousamadou.
 */
@InInterceptors
@Component
public class AppInboundInterceptor extends LoggingInInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppInboundInterceptor.class.getName());

    @Override
    public void handleMessage(Message message) throws Fault {
        LOGGER.info("PROCESSING MESSAGE AT IN-INTERCEPTOR");
        super.handleMessage(message);
    }
}
