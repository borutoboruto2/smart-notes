package fr.samadou.smartnotesbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartNotesBackendApplication  {

	public static void main(String[] args) {
		SpringApplication.run(SmartNotesBackendApplication.class, args);
	}
}
