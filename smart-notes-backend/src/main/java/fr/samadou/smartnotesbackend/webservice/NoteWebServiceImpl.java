package fr.samadou.smartnotesbackend.webservice;

import fr.samadou.smartnotesbackend.domain.Note;
import fr.samadou.smartnotesbackend.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author sareaboudousamadou.
 */
@Component
public class NoteWebServiceImpl implements NoteWebService{

    @Autowired
    private NoteService service;

    @Override
    public List<Note> fetchNotes() {
        List<Note> result = service.getAllNotes();

        return result!=null? result:Arrays.asList();
    }
}
