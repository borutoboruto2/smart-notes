package fr.samadou.smartnotesbackend.webservice;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author sareaboudousamadou.
 */
@javax.jws.WebService(serviceName = "HelloService", portName = "HelloPort",
        targetNamespace = "http://webservice.smartnotesbackend.samadou.fr/",
        endpointInterface = "fr.samadou.smartnotesbackend.webservice.Hello")
@Component
public class HelloPortImpl implements Hello {

    private static final Logger LOG = LoggerFactory.getLogger(HelloPortImpl.class.getName());

    public java.lang.String sayHello(java.lang.String myname) {
        LOG.info("Executing operation sayHello" + myname);
        try {
            return "Hello, Welcome to CXF Spring boot " + myname + "!!!";

        } catch (java.lang.Exception ex) {
            LOG.info("Exception - " + ex.getStackTrace());
            throw new RuntimeException(ex);
        }
    }

}