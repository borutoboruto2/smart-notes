package fr.samadou.smartnotesbackend.webservice;

/**
 * @author sareaboudousamadou.
 */

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 *
 */
@WebService(targetNamespace = "http://webservice.smartnotesbackend.samadou.fr/", name = "Hello")
public interface Hello {

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "sayHello",
            targetNamespace = "http://webservice.smartnotesbackend.samadou.fr/",
            className = "fr.samadou.smartnotesbackend.webservice.SayHello")
    @WebMethod(action = "urn:SayHello")
    @ResponseWrapper(localName = "sayHelloResponse",
            targetNamespace = "http://webservice.smartnotesbackend.samadou.fr/",
            className = "sample.ws.service.SayHelloResponse")
    String sayHello(@WebParam(name = "myname", targetNamespace = "") String myname);
}