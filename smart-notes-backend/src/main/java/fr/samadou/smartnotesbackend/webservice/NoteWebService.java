package fr.samadou.smartnotesbackend.webservice;

import fr.samadou.smartnotesbackend.domain.Note;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

/**
 * @author sareaboudousamadou.
 */
@WebService(serviceName = "NoteService")
public interface NoteWebService {

    @WebMethod(operationName = "fetchNotes")
    public List<Note> fetchNotes();
}
