import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { WeatherService } from './weather/weather-info/weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Smart Notes';

  constructor(private router: Router, private weatherService: WeatherService) {}

  displayWeather(): void {
    this.router.navigate([{outlets: {weather: ['weather']}}]);
    this.weatherService.displayedWeather = true;
  }

  hideWeather(): void {
    this.router.navigate([{outlets: {weather: null} }]);
    this.weatherService.displayedWeather = false;
  }

  get isWeatherDisplayed(): boolean {
    return this.weatherService.isDisplayedWeather();
  }
}
