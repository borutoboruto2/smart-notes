import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  weather = 'sunny';
  displayedWeather = false;
  constructor() { }

  getWeather(): string {
    return this.weather;
  }

  isDisplayedWeather(): boolean {
    return this.displayedWeather;
  }
}
