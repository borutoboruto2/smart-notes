import { Component, OnInit } from '@angular/core';
import { WeatherService } from './weather.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.css']
})
export class WeatherInfoComponent implements OnInit {
  weatherInfo: string;
  constructor(private weatherService: WeatherService,
              private router: Router) {
    this.weatherInfo = '';
  }

  ngOnInit() {
    this.weatherInfo = this.weatherService.getWeather();
  }

  close(): void {
    this.router.navigate([{outlets: { weather: null}}]);
    this.weatherService.displayedWeather = false;
  }

}
