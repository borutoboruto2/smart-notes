import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherInfoComponent } from './weather-info/weather-info.component';


const routes: Routes = [
  {
    path: 'weather',
    component: WeatherInfoComponent,
    outlet: 'weather'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule { }
