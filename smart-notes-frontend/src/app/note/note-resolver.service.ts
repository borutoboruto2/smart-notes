import { Injectable, OnDestroy } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NotesResolved } from './note';
import { NoteService } from './note.service';
import { Observable, of } from 'rxjs';
import { SubSink } from 'subsink';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class NoteResolverService implements Resolve<NotesResolved>, OnDestroy {

  private subs = new SubSink();
  constructor(private noteService: NoteService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<NotesResolved> {

    return this.noteService.getNotes()
      .pipe(
        map(noteReceive => ({ notes: noteReceive})),
        catchError(error => {
          const message = `Retrieval error: ${error}`;
          return of({ notes: [], error: message });
        })
      );

  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
