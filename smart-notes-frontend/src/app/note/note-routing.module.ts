import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteDetailComponent } from './note-detail/note-detail.component';
import { NoteDetailGuard } from './note-detail.guard';
import { NoteEditComponent } from './note-edit/note-edit.component';
import { NoteResolverService } from './note-resolver.service';


const ROUTES: Routes = [
  {
     path: '',
     component: NoteListComponent,
     resolve: { resolvedData: NoteResolverService }
  },
  {
    path: ':id/detail',
    canActivate: [NoteDetailGuard],
    component: NoteDetailComponent
  },
  {
    path: ':id/edit',
    component: NoteEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class NoteRoutingModule { }
