import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoteDetailGuard implements  CanActivate {

  constructor(private router: Router) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.isValidParameter(route);
  }


  isValidParameter(route: ActivatedRouteSnapshot): boolean {
    console.log(route.url);
    const id = +route.url[0].path;
    if (isNaN(id) || id < 1) {
      alert('Invalid product id');
      this.router.navigate(['notes']);
      return false;
    }
    return true;
  }
}
