import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteRoutingModule } from './note-routing.module';
import { NoteDetailComponent } from './note-detail/note-detail.component';
import { NoteEditComponent } from './note-edit/note-edit.component';



@NgModule({
  declarations: [NoteListComponent, NoteDetailComponent, NoteEditComponent],
  imports: [
    CommonModule,
    NoteRoutingModule
  ]
})
export class NoteModule { }
