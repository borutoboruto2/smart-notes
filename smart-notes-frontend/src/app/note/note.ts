import { Notecategory } from './notecategory.enum';

export interface Note {
     id: number;
     title: string;
     content: string;
     category: Notecategory;
}

export interface NotesResolved {
  notes: Note[];
  error?: any;
}
