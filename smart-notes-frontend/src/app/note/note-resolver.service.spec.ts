import { TestBed } from '@angular/core/testing';

import { NoteResolverService } from './note-resolver.service';
import { NoteService } from './note.service';

describe('NoteResolverService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [NoteService]
  }));

  it('should be created', () => {
    const service: NoteResolverService = TestBed.get(NoteResolverService);
    expect(service).toBeTruthy();
  });
});
