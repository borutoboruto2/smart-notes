import { Injectable } from '@angular/core';
import { Note } from './note';
import { Notecategory } from './notecategory.enum';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  private notesApiUrls = 'http://127.0.0.1:8080/notes';

  constructor(private httpClient: HttpClient) { }

  getNotes(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(this.notesApiUrls);
  }
}
