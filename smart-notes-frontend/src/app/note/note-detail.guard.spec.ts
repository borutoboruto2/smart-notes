import { TestBed, async, inject } from '@angular/core/testing';

import { NoteDetailGuard } from './note-detail.guard';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('NoteDetailGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoteDetailGuard],
      imports: [RouterTestingModule]
    });
  });

  it('should ...', inject([NoteDetailGuard], (guard: NoteDetailGuard) => {
    expect(guard).toBeTruthy();
  }));
});
