import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule , HttpTestingController} from '@angular/common/http/testing';

import { NoteService } from './note.service';
import { Notecategory } from './notecategory.enum';

describe('NoteService', () => {
  let httpTesingController: HttpTestingController;
  let noteService: NoteService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  httpTesingController = TestBed.get(HttpTestingController);
  noteService = TestBed.get(NoteService);

  it('should be created', () => {
    const service: NoteService = TestBed.get(NoteService);

    expect(service).toBeTruthy();
  });


  describe('getNotes', () =>
  it('should call getNotes with the correct URL', ()  => {
        noteService.getNotes().subscribe();
        const request = httpTesingController.expectOne('/notes');
        request.flush({id: 1, title: 'My note', content: 'It is smart note', category: Notecategory.JAVA});
  }));

});


