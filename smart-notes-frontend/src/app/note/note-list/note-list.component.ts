import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Note, NotesResolved } from '../note';
import { NoteService } from '../note.service';
import { SubSink } from 'subsink';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit, OnDestroy {
  private subSink = new SubSink();
  pageTitle = 'List of notes';

  notes: Note[];
  errorMessage = '';

  constructor(private route: ActivatedRoute) {
    this.notes = [];
  }

  ngOnInit() {
    this.subSink.add(this.route.data.subscribe(
      datas => {
        const resolvedData: NotesResolved = datas.resolvedData;
        this.errorMessage = resolvedData.error;
        this.notes = resolvedData.notes;
      }
    ));
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
