import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SelectiveStrategyService } from './selective-strategy.service';

const ROUTES: Routes = [
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'notes',
    loadChildren: './note/note.module#NoteModule',
    data: {preload: true}
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
  ];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES , {preloadingStrategy: SelectiveStrategyService, enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
